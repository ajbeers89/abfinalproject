package com.finalproject.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTestDom is a Querydsl query type for TestDom
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTestDom extends EntityPathBase<TestDom> {

    private static final long serialVersionUID = -1938664060L;

    public static final QTestDom testDom = new QTestDom("testDom");

    public final StringPath firstName = createString("firstName");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath lastName = createString("lastName");

    public QTestDom(String variable) {
        super(TestDom.class, forVariable(variable));
    }

    public QTestDom(Path<? extends TestDom> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTestDom(PathMetadata metadata) {
        super(TestDom.class, metadata);
    }

}

