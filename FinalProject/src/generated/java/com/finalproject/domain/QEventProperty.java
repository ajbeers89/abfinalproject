package com.finalproject.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEventProperty is a Querydsl query type for EventProperty
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEventProperty extends EntityPathBase<EventProperty> {

    private static final long serialVersionUID = 1460449731L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEventProperty eventProperty = new QEventProperty("eventProperty");

    public final QEvent event;

    public final StringPath eventKey = createString("eventKey");

    public final StringPath eventValue = createString("eventValue");

    public final NumberPath<Integer> propertyId = createNumber("propertyId", Integer.class);

    public QEventProperty(String variable) {
        this(EventProperty.class, forVariable(variable), INITS);
    }

    public QEventProperty(Path<? extends EventProperty> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEventProperty(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEventProperty(PathMetadata metadata, PathInits inits) {
        this(EventProperty.class, metadata, inits);
    }

    public QEventProperty(Class<? extends EventProperty> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.event = inits.isInitialized("event") ? new QEvent(forProperty("event")) : null;
    }

}

