package com.finalproject.util;

import java.sql.Date;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.finalproject.dao.EventPropertyRepository;
import com.finalproject.dao.EventRepository;
import com.finalproject.domain.Event;
import com.finalproject.domain.EventProperty;
import com.finalproject.domain.IssLocationResponse;

@Component
public class ScheduledTasks {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            .withZone(ZoneId.systemDefault());
 
    @Autowired
    EventRepository eventRepo;
    @Autowired
    EventPropertyRepository eventPropertyRepo;

    @Scheduled(fixedRate = 30000)    
    public void gatherFooData() {       
        
        Instant now = Instant.now();
        String startTimeString = formatter.format(now);
        String serviceUri = "http://api.open-notify.org/iss-now.json";
        RestTemplate restTemplate = new RestTemplate();
        // Call the service and populate the Response Object
        IssLocationResponse responseJSON = restTemplate.getForObject(serviceUri, IssLocationResponse.class); 
        
        // Build an instance of the parent entity object
        // Populate parent object values and call parentRepo.save(parentObj) (Saving over and over again with the same data is fine)
        Event event = new Event(startTimeString,"IssLocation","N");
        event = eventRepo.save(event);
        
        // Build an instance of the child object
        // Populate child object values and call childRepo.save(childObj)
        List<EventProperty> properties = new ArrayList<EventProperty>();
        EventProperty longitude = new EventProperty("longitude",responseJSON.getIssPosition().getLongitude(),event);
        EventProperty latidute = new EventProperty("latitude",responseJSON.getIssPosition().getLatitude(),event);
        EventProperty message = new EventProperty("message",responseJSON.getMessage(),event);
        properties.add(longitude);
        properties.add(latidute);
        properties.add(message);
        eventPropertyRepo.saveAll(properties);
     
      //  }
    }
}
