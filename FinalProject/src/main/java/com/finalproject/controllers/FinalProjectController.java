package com.finalproject.controllers;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finalproject.domain.Event;
import com.finalproject.dto.IssEventDto;
import com.finalproject.service.FinalProjectService;
import com.finalproject.util.ScheduledTasks;

@Controller
@EnableScheduling
@RequestMapping("/")
public class FinalProjectController extends BaseController{

   @Autowired
   FinalProjectService finalProjectService;
   
   @Autowired
   ScheduledTasks scheduledTask;

    // main page load
    @GetMapping(value = {"finalproject"})
    public String loadTest(Model model) throws Exception{
        return "finalProject";
    }
    
    // call to send sms of events checked
    @ResponseBody
    @PostMapping(value = { "finalproject/xSend" })
    public String getXSend(Model model,IssEventDto issEventDto) throws Exception{
        // AJAX CALL
        try {
            String message = finalProjectService.sendSms(issEventDto);
            model.addAttribute("successMessage", message);
            return message;
        }catch(Exception e) {
            return "Unexpected Error: " + e.toString(); 
        }
          
    }
    // Loads database into grid
    @ResponseBody
    @GetMapping(value = "finalproject/loadGrid")
    public String getXJackson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String event = mapper.writeValueAsString(finalProjectService.getData());
        return event;
    }
}
