package com.finalproject.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.finalproject.service.TestService;

@Controller
@RequestMapping("/")
public class TestController extends BaseController{

   @Autowired
   TestService testService;
    
    @GetMapping(value = {"test"})
    public String loadTest(Model model) throws Exception{
        String testResult= "THIS IS A TEEST";
        model.addAttribute("message", testResult);
        return "test";
    }
}
