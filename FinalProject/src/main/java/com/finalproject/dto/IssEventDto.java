package com.finalproject.dto;

import java.io.Serializable;

public class IssEventDto implements Serializable {

    private static final long serialVersionUID = 2015968310033442854L;
    private String phoneNumber;
    private String jsonStuff;
   

    public IssEventDto() {}


    public IssEventDto(String phoneNumber, String jsonStuff) {
        super();
        this.phoneNumber = phoneNumber;
        this.jsonStuff = jsonStuff;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }


    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public String getJsonStuff() {
        return jsonStuff;
    }


    public void setJsonStuff(String jsonStuff) {
        this.jsonStuff = jsonStuff;
    }


    @Override
    public String toString() {
        return "IssEventDto2 [phoneNumber=" + phoneNumber + ", jsonStuff=" + jsonStuff + "]";
    }
    
   


 

   

    // Add getters and setters...
}
