package com.finalproject.domain;
import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.validation.constraints.NotNull;


import org.hibernate.validator.constraints.NotEmpty;

@Entity 
@Table(name="events")
public class Event implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5239138953183565138L;

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EVENT_ID", columnDefinition = "INTEGER")
    private Integer eventId;

    @NotNull
    @Column(name = "DATE_TIME", columnDefinition = "TIMESTAMP")
    private String dateTime;
    
    @NotNull
    @Column(name = "EVENT_TYPE", columnDefinition = "VARCHAR(50)")
    private String eventType;
    
    @NotNull
    @Column(name = "SMS_SENT", columnDefinition = "VARCHAR(1)")
    private String smsSent;


    @OneToMany(fetch= FetchType.EAGER, mappedBy = "event",cascade=CascadeType.MERGE)
    private List<EventProperty>eventProprtyList = new ArrayList<EventProperty>();
    public Event() {
        
    }
    public Event(String dateTime, String eventType,String smsSent) {
        this.dateTime = dateTime;
        this.smsSent = smsSent;
        this.eventType=eventType;
    }
    
    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }

    public List<EventProperty> getEventProprtyList() {
        return eventProprtyList;
    }

    public void setEventProprtyList(List<EventProperty> eventProprtyList) {
        this.eventProprtyList = eventProprtyList;
    }
    public String getMessage() {
        String message ="";
        for (EventProperty eventProperty : eventProprtyList) {
            if("message".equalsIgnoreCase(eventProperty.getEventKey())) {
                return eventProperty.getEventValue();
            }
        }
        return message;
    }
    public String getLongitude() {
        String longitude ="";
        for (EventProperty eventProperty : eventProprtyList) {
            if("longitude".equalsIgnoreCase(eventProperty.getEventKey())) {
                return eventProperty.getEventValue();
            }
        }
        return longitude;
    }
    public String getLatitude() {
        String latitude ="";
        for (EventProperty eventProperty : eventProprtyList) {
            if("latitude".equalsIgnoreCase(eventProperty.getEventKey())) {
                return eventProperty.getEventValue();
            }
        }
        return latitude;
    }
    @Override
    public String toString() {
        return "Event [eventId=" + eventId + ", dateTime=" + dateTime + ", eventType=" + eventType + ", smsSent=" + smsSent
                + ", eventProprtyList=" + eventProprtyList + "]";
    }
    
}
