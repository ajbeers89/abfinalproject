package com.finalproject.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class IssPosition{
    private String longitude;
    private String latitude;
    
   
    public IssPosition() {
        
    }


    public String getLongitude() {
        return longitude;
    }


    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public String getLatitude() {
        return latitude;
    }


    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }


    @Override
    public String toString() {
        return "IssPosition [longitude=" + longitude + ", latitude=" + latitude + "]";
    }
    
    
}