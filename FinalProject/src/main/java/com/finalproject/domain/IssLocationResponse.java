package com.finalproject.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class IssLocationResponse{
    @JsonProperty("timestamp")
	private String timeStamp;
    @JsonProperty("message")
	private String message;
	@JsonProperty("iss_position")
	private IssPosition issPosition;
	
	public IssLocationResponse(){
	}

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IssPosition getIssPosition() {
        return issPosition;
    }

    public void setIssPosition(IssPosition issPosition) {
        this.issPosition = issPosition;
    }

    @Override
    public String toString() {
        return "IssLocationResponse [timeStamp=" + timeStamp + ", message=" + message + ", issPosition=" + issPosition + "]";
    }

}