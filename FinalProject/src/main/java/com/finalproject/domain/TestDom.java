package com.finalproject.domain;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;


@Entity 
@Table(name="test")
public class TestDom implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5239138953183565138L;

    @Id 
    @Column(name = "id", columnDefinition = "INTEGER")
    private Integer id;

    @NotNull
    @NotEmpty
    @Column(name = "first_name", columnDefinition = "VARCHAR(32)")
    private String firstName;
    
    @NotNull
    @NotEmpty
    @Column(name = "last_name", columnDefinition = "VARCHAR(32)")
    private String lastName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

 
}
