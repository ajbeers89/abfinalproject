package com.finalproject.domain;

import java.io.Serializable;
import java.util.Arrays;

public class IssEventList implements Serializable {

    private static final long serialVersionUID = 2015968310033442854L;
    private IssEvent[] jsonData;


    public IssEventList() {}


    public IssEvent[] getJsonData() {
        return jsonData;
    }


    public void setJsonData(IssEvent[] jsonData) {
        this.jsonData = jsonData;
    }


    @Override
    public String toString() {
        return "IssEventList [jsonData=" + Arrays.toString(jsonData) + "]";
    }
    
    



    

   

    // Add getters and setters...
}
