package com.finalproject.domain;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity 
@Table(name="event_properties")
public class EventProperty implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5239138953183565138L;

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PROPERTY_ID", columnDefinition = "INTEGER")
    private Integer propertyId;
    
    @NotNull
    @Column(name = "EVENT_KEY", columnDefinition = "VARCHAR(50)")
    private String eventKey;
    
    @NotNull
    @Column(name = "EVENT_VALUE", columnDefinition = "VARCHAR(160)")
    private String eventValue;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "EVENT_ID", referencedColumnName = "EVENT_ID", columnDefinition ="Integer")
    private Event event;
    public EventProperty() {
        
    }
    public EventProperty(String key, String value,Event eventId) {
        eventKey=key;
        eventValue=value;
        event=eventId;
    }
    
    public Integer getPropertyId() {
       
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }


    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public String getEventValue() {
        return eventValue;
    }

    public void setEventValue(String eventValue) {
        this.eventValue = eventValue;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
    
    
 
}
