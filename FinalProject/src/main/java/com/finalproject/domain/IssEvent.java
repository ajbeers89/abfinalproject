package com.finalproject.domain;

import java.io.Serializable;

public class IssEvent implements Serializable {

    private static final long serialVersionUID = 2015968310033442854L;
    private Integer eventId;
    private String dateTime;
    private String longitude;
    private String latitude;
    private String smsSent;

    public IssEvent() {}

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }

    @Override
    public String toString() {
        return "IssEvent [eventId=" + eventId + ", dateTime=" + dateTime + ", longitude=" + longitude + ", latitude=" + latitude
                + ", smsSent=" + smsSent + "]";
    }

    

  
 

   

    // Add getters and setters...
}
