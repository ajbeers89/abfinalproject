package com.finalproject.dao;

import org.springframework.data.repository.CrudRepository;

import com.finalproject.dao.custom.TestRepositoryCustom;
import com.finalproject.domain.TestDom;

public interface TestRepository extends CrudRepository<TestDom,Integer>, TestRepositoryCustom {
        // TODO Auto-generated constructor stub
    }


