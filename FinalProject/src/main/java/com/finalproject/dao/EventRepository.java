package com.finalproject.dao;

import org.springframework.data.repository.CrudRepository;

import com.finalproject.dao.custom.EventRepositoryCustom;
import com.finalproject.domain.Event;


public interface EventRepository extends CrudRepository<Event,Integer>, EventRepositoryCustom {
        Event findByEventId(Integer eventId);
    
        // TODO Auto-generated constructor stub
    }


