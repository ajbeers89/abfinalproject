package com.finalproject.dao.impl;


import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.finalproject.dao.custom.TestRepositoryCustom;
import com.finalproject.domain.TestDom;

@Repository
public class TestRepositoryImpl extends QuerydslRepositorySupport implements TestRepositoryCustom {

    public TestRepositoryImpl() {
        super(TestDom.class);
        // TODO Auto-generated constructor stub
        
    }

}
