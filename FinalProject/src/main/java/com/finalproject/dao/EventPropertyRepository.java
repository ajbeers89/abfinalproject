package com.finalproject.dao;

import org.springframework.data.repository.CrudRepository;
import com.finalproject.dao.custom.EventPropertyRepositoryCustom;
import com.finalproject.domain.EventProperty;


public interface EventPropertyRepository extends CrudRepository<EventProperty,Integer>, EventPropertyRepositoryCustom {

    
        // TODO Auto-generated constructor stub
    }


