package com.finalproject.service;

public interface TwilioService{
    boolean isLandLine(String number);
    String sendSms(String number, String body);    
}