package com.finalproject.service.impl;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.finalproject.service.TwilioService;
import com.finalproject.service.impl.FinalProjectServiceImpl;
import com.twilio.Twilio;
import com.twilio.converter.Promoter;
import com.twilio.rest.lookups.v1.PhoneNumber;
@Service("TwilioService")
public class TwilioServiceImpl implements TwilioService {
    private static final String LANDLINE = "landline";
    private static final String ANDREW_CELL ="+12175567866";
    private static final String MITCH_CELL = "+14175228001";
    private static final String NICK_CELL = "+14175974289";
    private static final String JEFFERY_CELL = "+14174239219";
    private static final String TWILIO_CELL ="+16185857171";
    private String ACCOUNT_SID = null;
    private String AUTH_TOKEN = null;
    
    public TwilioServiceImpl() throws IOException{
        Properties appProperties = new Properties();
        appProperties.load(FinalProjectServiceImpl.class.getClassLoader().getResourceAsStream("app.properties"));
        ACCOUNT_SID = appProperties.getProperty("twilio.account.sid");
        AUTH_TOKEN = appProperties.getProperty("twilio.token");
        
    }
    @Override
    public boolean isLandLine(String number) {
        number = number.replaceAll("[^\\d.]", "");
        number = (number.length() == 11) ? ("+" + number) : ("+1" + number);
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        PhoneNumber phoneNumber = PhoneNumber.fetcher(new com.twilio.type.PhoneNumber(number))
                .setCountryCode("US")
                .setType(Promoter.listOfOne("carrier"))
                .fetch();

        // Get the map of details from the PhoneNumber response
        Map<String, String> carrierMap = phoneNumber.getCarrier();

        // Can't send SMS messages to LandLine numbers
        if (LANDLINE.equalsIgnoreCase(carrierMap.get("type"))) {
            return true;
        }
        return false;
    }
    @Override
    public String sendSms(String number, String body) {
        //check if landline
        if (isLandLine(number)) {
            return "failed (number is a landline)";
        }

       //known/good number, send mgs
        if(number.equals(ANDREW_CELL)||number.equals(NICK_CELL)||number.equals(JEFFERY_CELL)||number.equals(MITCH_CELL)) {
            String url = "https://api.twilio.com/2010-04-01/Accounts/" + ACCOUNT_SID + "/Messages.json";
          // setup auth and encode
            String auth = ACCOUNT_SID + ":" + AUTH_TOKEN;
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
          //create request head
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.set("Authorization", "Basic " + new String(encodedAuth));
        
         // Create Request Body (Payload)
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("To", number);
            params.add("From", TWILIO_CELL);
            params.add("Body", body);
            // Send The Request to the Web Service and Print the Response
            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
            return "sent";
        } else {
            return "failed";
        } 
    }
}