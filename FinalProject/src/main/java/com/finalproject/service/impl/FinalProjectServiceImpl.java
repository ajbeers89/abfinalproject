package com.finalproject.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finalproject.dao.EventRepository;
import com.finalproject.domain.Event;
import com.finalproject.domain.IssEvent;
import com.finalproject.domain.IssEventList;
import com.finalproject.dto.IssEventDto;
import com.finalproject.service.FinalProjectService;
import com.finalproject.service.TwilioService;

@Service("FinalProjectService")
public class FinalProjectServiceImpl implements FinalProjectService {
    
    @Autowired
    TwilioService twilioService;
    @Autowired
    EventRepository eventRepository;

    @Override
    public List<Event> getData() {
        List<Event> eList = (List<Event>) eventRepository.findAll();
        return eList;
    }

    // main SMS service call
    @Override
    public String sendSms(IssEventDto issEventDto)throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        IssEventList issEventList = objectMapper.readValue(issEventDto.getJsonStuff(), IssEventList.class);
        
        try {
           String number = issEventDto.getPhoneNumber();
          // number = number.replace("-", "");
           if(number != null) {
               number = number.replaceAll("[^0-9]",""); 
               if(number.length()==11) {
                   if(number.startsWith("1")) {
                       number = number.substring(1);
                   }
               }
               if(number.length()==10) {
                   String status = "Please select an event to send.";
                   Integer textNumber =0;
                   for (IssEvent issEvent : issEventList.getJsonData()) {
                       Event event = eventRepository.findByEventId(issEvent.getEventId());
                       if("N".equals(event.getSmsSent())) {
                           textNumber++;
                           String body = buildBody(issEvent,textNumber);
                           status = twilioService.sendSms("+1"+number, body);
                           
                       }
                   }
                   return checkStatus(status,issEventList);
               }
           }
           return "Not a complete Number. Please try again.";
        }
        catch(Exception e) {return e.toString();}
    }
    
    // builds text body from checked events limited to 122 characters plus the 38 inserted by twillio
    @Override
    public String buildBody(IssEvent issEvent, Integer textNumber)throws Exception {
        StringBuilder sBuilder = new StringBuilder();
        sBuilder.append(" Event Update: " + textNumber);
        sBuilder.append(" Iss Location Event Id: " + issEvent.getEventId());
        sBuilder.append(" Longitude: " + issEvent.getLongitude());
        sBuilder.append(" Latitude: " + issEvent.getLatitude());

        String body = sBuilder.toString();
        if(body.length()>122) {
            body = body.substring(0, 119);
            body = body+"...";
        }
        return body;
    }
    
    // checks for status 
    @Override
    public String checkStatus(String status,IssEventList issEventList)throws Exception {
        switch(status) {
            case "sent":
                updateSmsSent(issEventList);
                break;
            case "failed (number is a landline)":
                break;
            case "failed":    
        }
        return status;   
    }
    // updates sms field on database
    @Override
    public void updateSmsSent(IssEventList issEventList) throws Exception{
        for (IssEvent issEvent : issEventList.getJsonData()) {
            Integer tempId = issEvent.getEventId();
            Event event = eventRepository.findById(tempId).get();
            event.setSmsSent("Y");
            eventRepository.save(event);
          }
    }
}
