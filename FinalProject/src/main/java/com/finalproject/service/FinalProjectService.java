package com.finalproject.service;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.finalproject.domain.Event;
import com.finalproject.domain.IssEvent;
import com.finalproject.domain.IssEventList;
import com.finalproject.domain.IssLocationResponse;
import com.finalproject.dto.IssEventDto;

public interface FinalProjectService {
    
    String sendSms(IssEventDto issEvent)throws Exception;


    List<Event> getData();
    String checkStatus(String status, IssEventList issEventList) throws Exception;
    void updateSmsSent(IssEventList issEventList) throws Exception;
    String buildBody(IssEvent issEvent, Integer textNumber) throws Exception;

    
}

