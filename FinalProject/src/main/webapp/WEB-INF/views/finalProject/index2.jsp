<%@ include file="/WEB-INF/layouts/include.jsp"%>
<div>
	<h1>ISS SPACE STATION CURRENT LOCATION</h1>
	<div class="row">
		<div class="col-sm-12">
			<div class="well" id="well">
				<h2>Description</h2>
				<p>This page shows the International Space Stations's Current Latitude and Longitude. </p>
				<p>To send Longitude and Latitude information about the ISS enter a valid mobile number. <br />Then check the events you would like to send records of.</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div data-dojo-type="oreilly/types/ui/AlertManager" id="am">${successMessage}</div>
		</div>
	</div>

	
		<form method="post" action="#" data-dojo-type="dijit/form/Form" id="userForm">
			<div class="row">
				<div class="col-sm-8">
				</div>
				<div class="col-sm-3">
					<input class="text pull-right" id = "number" name="number" type="number" placeholder='###-###-####'
						data-dojo-type="oreilly/types/form/ValidationTextarea" />
		  	</div>
				<div class="col-sm-1">
					<button class="btn btn-primary pull-right" id="sendButton" type="button"
						data-dojo-type="oreilly/types/form/Button"
						data-dojo-props="spinOnClick: true">Send</button>
				</div>
			</div> 
		</form>
	
	<div class="row">
		<div class="col-sm-12"  id="tbl">
			<div class="mt10" id="objectMapperTable">
				<div data-dojo-id="objectMapperStore"
					data-dojo-type="dojo/store/Memory"
					data-dojo-props="data: [], idProperty: 'eventId'"></div>
				<table id="objectMapperGrid"
					class="table table-striped table-bordered"
					data-dojo-type="oreilly/types/dgrid/PagingGridCheckBox"
					data-dojo-id="checkedboxGrid"
					data-dojo-props="store: objectMapperStore, loadDataOnStartup: true">
					<thead>
						<tr>
							<th class="hyperlink"
								data-dgrid-column='{ field:"eventId", name: "eventId", sortable: true}'>EVENT ID
								</th>
							<th class="hidden"
								data-dgrid-column='{ field:"smsSent", name: "smsSent", sortable: true}'>
								</th>
							<th class="hyperlink"
								data-dgrid-column='{ field:"dateTime", name:"dateTime",sortable: true}'>EVENT DATE</th>
							<th class="hyperlink"
								data-dgrid-column='{ field:"message",name: "message", sortable: true}'>STATUS</th>
							<th class="hyperlink"
								data-dgrid-column='{ field:"longitude", name: "longitude", sortable: true}'>LONGITUDE</th>
							<th class="hyperlink"
								data-dgrid-column='{ field:"latitude", name: "latitude", sortable: true}'>LATITUDE</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
	require([ 'dijit/registry', 'dojo/request', 'dojo/dom', 'dojo/ready','dojo/on' ,'dojo/dom-construct','dijit/form/CheckBox'],
			function(registry, request, dom, ready, on,domConstruct,checkBox) {
				var alertManager;
			
				ready(function() {

					// Variable Declaration
					var sentArray=[];

					var table = dom.byId("tbl");
					var sendButton = registry.byId("sendButton");
					var alertManager = registry.byId('alertManager');
					var objectMapperGrid = registry.byId("objectMapperGrid");
					var objectMapperStore = registry.byId("objectMapperStore");
					// reload table every 30 sec
					var interval = setInterval(function () { loadGrid(); }, 30000);
					
					// Load the Table after the DOM is ready
					loadGrid();
					// load Table function
					function loadGrid(){
						request('<c:url value="/finalproject/loadGrid" />', {
							method : 'GET',
							handleAs : "json",
							data : {}
						}).then(function(data) {
							let myJson = JSON.parse(data);
							objectMapperGrid.store.setData(myJson);
							objectMapperGrid.refresh();
							addSent(myJson);
							deleteCheckboxes();
						}, function(err) {
							alertManager.addError({
								message : err,
								position : 'alertManager'
							});
						}); 
					};
					function showMessage(data){
						if(data =="sent"){
							alertManager.addSuccess({message: "Text Sent Successfully.", position: 'am', duration:5000});
						}
						else if(data == "failed"){
							alertManager.addError({message: "This Number is not verified. Please verify with Twilio and try again.", position: 'am', duration:5000});
						}
						else if(data == "failed (number is a landline)"){
							alertManager.addError({message: "This is a Landline number, Please enter a mobile number", position: 'am', duration:5000});
						}
						else{
							alertManager.addError({message: data, position: 'am', duration:5000});
						}

					}
					function addSent(myJson){
						try{
							sentArray=[];
							for(i in myJson){
								if(myJson[i].smsSent =="Y"){
									sentArray.push(myJson[i].eventId);
								}
							}
						}catch(err){
						}	
					}
					// destroy checkboxes  ****************NOT WORKING
					
					function deleteCheckboxes(){
						try{
							for(sent in sentArray){
									let idString = "oap-checkbox-"+sentArray[sent]+"objectMapperGrid";
									if( registry.byId(idString)!= undefined){
										let box = registry.byId(idString).destroy();
									}
							}
						}catch(err){
						}
					}
					
				
					// table click refresh checkboxes

				 	on(table,'click', function (e) {
				 		for(sent in sentArray){
							let idString = "oap-checkbox-"+sentArray[sent]+"objectMapperGrid";
							if( dijit.byId(idString)!= undefined){
								let box = dijit.byId(idString);
								//box.setAttribute('aria-checked',false);
								box.set("value",false);
								box.destroy();
							}
						}
						deleteCheckboxes();
					}); 
					
					
					// AJAX Button call to send sms
					 registry.byId('sendButton').on('click', function (e) {
						let phoneNumber = registry.byId("number").value;
						e.preventDefault();
						let json={};
						let myArray=[];
						let checkedArray=[];
						checkedArray = objectMapperGrid.getChecked();
						for (var i = 0; i < checkedArray.length; i++) {
							let obj={};
							obj.eventId = checkedArray[i].eventId;
							obj.smsSent = checkedArray[i].smsSent;
							obj.dateTime = checkedArray[i].dateTime;
							obj.longitude = checkedArray[i].longitude;
							obj.latitude = checkedArray[i].latitude;
							myArray.push(obj);
						}
						json.jsonData=myArray;
						var btn = this;
						request('<c:url value="/finalproject/xSend" />', {
							method: 'POST',
							handleAs: "json",
							data: {
								'phoneNumber': phoneNumber,
								'jsonStuff': JSON.stringify(json)
							}
						}).then(function(data) {
							var msg;
							btn.stopSpinner();
							dom.byId('number').value ="";
							showMessage(data);
							loadGrid();
						}, function(err) {
							btn.stopSpinner();
						    alertManager.addError({message: err, position: 'am'});
						});
					}); // end registry by id 
					

				}); // end ready function
			}); // end require function
</script>